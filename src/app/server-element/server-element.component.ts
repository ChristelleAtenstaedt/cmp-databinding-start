import {
  Component,
  Input,
  SimpleChanges,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  ContentChild,
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrl: './server-element.component.css',
  // The below means that any styles defined in the css file will be applied globally
  // encapsulation: ViewEncapsulation.None,
})
export class ServerElementComponent {
  // @Input() lets a parent component update data in the child component.
  @Input('srvElement') element: { type: string; name: string; content: string };
  @Input() name: 'string';
  @ViewChild('heading') header: ElementRef;
  // get access to content which is stored in another componenet but passed on via ngContent
  @ContentChild('contentParagraph') paragraph: ElementRef;

  constructor() {
    console.log('constructor called!');
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges called!');
    console.log(changes);
  }
  ngOnInit() {
    console.log('ngOnInit called!');
    // console.log('Text content' + this.header.nativeElement.textContent);
    // console.log(
    //   'Text content of paragraph' + this.paragraph.nativeElement.textContent
    // );
  }

  ngDoCheck() {
    console.log('ngDoCheck called!');
  }
  ngAfterContentInit() {
    console.log('ngAfterContentInit called!');
    console.log(
      'Text content of paragraph: ' + this.paragraph.nativeElement.textContent
    );
  }
  ngAfterContentChecked() {
    console.log('ngAfterContentChecked called!');
  }
  // gives you access to the template elements
  ngAfterViewInit() {
    console.log('AfterViewInit called!');
    console.log('Text content' + this.header.nativeElement.textContent);
  }
  ngAfterViewChecked() {
    console.log('ngAfterViewChecked called!');
  }
  ngOnDestroy() {
    console.log('ngOnDestroyed called!');
  }
}

// all properties of components are only accessible from within that component. if you want to allow parent componnet to bind to it then you need to add a decorator. With @Input() we are now exposing it.
